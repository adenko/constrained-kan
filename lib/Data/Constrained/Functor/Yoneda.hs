module Data.Constrained.Functor.Yoneda where 

import Data.Constrained.Functor.Contravariant
    ( ConstraintContravariant(..) )

newtype YonedaC f c a = YonedaC { runYonedaC :: forall b . c b => (a -> b) -> f b} deriving Functor

lowerYonedaC :: c a => YonedaC f c a -> f a 
lowerYonedaC (YonedaC x) = x id 

liftYonedaC :: Functor f => f a -> YonedaC f c a 
liftYonedaC x = YonedaC (<$> x)

instance ConstraintContravariant (YonedaC f) where 
    strengthenConstraint (YonedaC f) = YonedaC f 
