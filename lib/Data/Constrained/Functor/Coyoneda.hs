module Data.Constrained.Functor.Coyoneda where 

import Data.Constrained.Functor


data CoyonedaC f c a where 
    CoyonedaC :: c b => (b -> a) -> f b -> CoyonedaC f c a
deriving instance Functor (CoyonedaC f c)

instance ConstraintFunctor (CoyonedaC f) where 
    weakenConstraint (CoyonedaC a b) = CoyonedaC a b

lowerCoyonedaC :: Functor f => CoyonedaC f c a -> f a 
lowerCoyonedaC (CoyonedaC a b) = a <$> b 

liftCoyonedaC :: c a => f a -> CoyonedaC f c a
liftCoyonedaC = CoyonedaC id 

