module Data.Constrained.Functor.Contravariant where 

import Data.Functor.Contravariant ( Contravariant(contramap) ) 
import Data.Kind(Type, Constraint)
import Data.Unconstrained ( Unconstraining, Unconstrained ) 

class ConstrainedContravariant f where 
    type ContravariantConstraint f :: Type -> Constraint  
    contramapc :: (ContravariantConstraint f a, ContravariantConstraint f b) => (b -> a) -> f a -> f b 
instance Contravariant f => ConstrainedContravariant (Unconstraining f) where 
    type ContravariantConstraint (Unconstraining f) = Unconstrained 
    contramapc = contramap  

class ConstraintContravariant f where 
    strengthenConstraint :: (forall x . c x => c' x) => f c' a -> f c a 
