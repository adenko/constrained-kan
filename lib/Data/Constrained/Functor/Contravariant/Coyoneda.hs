module Data.Constrained.Functor.Contravariant.Coyoneda where

import Data.Functor.Contravariant ( Contravariant(contramap) )

data OpcoyonedaC f c a where 
    OpcoyonedaC :: c b => (a -> b) -> f b -> OpcoyonedaC f c a 
instance Contravariant (OpcoyonedaC f c) where 
    contramap f (OpcoyonedaC a b) = OpcoyonedaC (a . f) b 

lowerOpcoyonedaC :: Contravariant f => OpcoyonedaC f c a -> f a 
lowerOpcoyonedaC (OpcoyonedaC a b) = contramap a b 

liftOpcoyonedaC :: c a => f a -> OpcoyonedaC f c a
liftOpcoyonedaC = OpcoyonedaC id