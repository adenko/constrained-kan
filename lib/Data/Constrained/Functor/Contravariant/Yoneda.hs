module Data.Constrained.Functor.Contravariant.Yoneda where

import Data.Functor.Contravariant ( Contravariant(contramap) ) 

newtype OpyonedaC f c a = OpyonedaC { runOpyonedaC :: forall b . c b => (b -> a) -> f b }
instance Contravariant (OpyonedaC f c) where 
    contramap f (OpyonedaC x) = OpyonedaC (x . fmap f)
lowerOpyonedaC :: c a => OpyonedaC f c a -> f a 
lowerOpyonedaC (OpyonedaC x) = x id 

liftOpyonedaC :: Contravariant f => f a -> OpyonedaC f c a
liftOpyonedaC x = OpyonedaC (`contramap` x)
