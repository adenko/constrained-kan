module Data.Constrained.Functor.Kan.Ran where 
    
import Data.Constrained.Functor.Contravariant
    ( ConstraintContravariant(..) ) 

newtype RanC f g c a = RanC 
    { runRanC :: forall b . c b => (a -> f b) -> g b } deriving Functor 

instance ConstraintContravariant (RanC f g) where 
    strengthenConstraint = toRanC granC

granC :: c a => RanC g h c (g a) -> h a 
granC (RanC f) = f id 

toRanC :: Functor k => (forall a . c a => k (g a) -> h a) -> k b -> RanC g h c b 
toRanC f x = RanC (f . (<$> x))

fromRanC :: c b => (forall a . k a -> RanC g h c a) -> k (g b) -> h b 
fromRanC f x = runRanC (f x) id
