module Data.Constrained.Functor.Kan.Lan where 

import Data.Constrained.Functor

data LanC f g c a where 
    LanC :: c b => (f b -> a) -> g b -> LanC f g c a 
deriving instance Functor (LanC f g c)

instance ConstraintFunctor (LanC f g) where 
    weakenConstraint = toLanC glanC

glanC :: c a => h a -> LanC g h c (g a)
glanC x = LanC id x

toLanC :: Functor f => (forall a . c a => h a -> f (g a)) -> LanC g h c b -> f b 
toLanC s (LanC f v) = f <$> s v

fromLanC :: c b => (forall a . LanC g h c a -> f a) -> h b -> f (g b)
fromLanC s = s . glanC 
