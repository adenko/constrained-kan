module Data.Constrained.Bifunctor where 

import Data.Bifunctor ( Bifunctor(bimap) ) 
import Data.Kind ( Constraint ) 
import Data.Unconstrained ( Unconstraining2 ) 

class ConstraintBifunctor b where 
    weakenConstraints :: (forall x . c x => c' x, forall x . d x => d' x) => b c d x y -> b c' d' x y 
class ConstrainedBifunctor b where 
    type BifunctorConstraint b x y :: Constraint 
    bimapC :: (BifunctorConstraint b x y, BifunctorConstraint b x' y) => (x -> x') -> (y -> y') -> b x y -> b x' y'
instance Bifunctor b => ConstrainedBifunctor (Unconstraining2 b) where 
    type BifunctorConstraint (Unconstraining2 b) x y = () 
    bimapC = bimap 