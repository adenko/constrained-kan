module Data.Constrained.Functor where

import Data.Kind ( Type, Constraint ) 
import Data.Unconstrained ( Unconstraining, Unconstrained ) 

class ConstrainedFunctor f where 
    type FunctorConstraint f :: Type -> Constraint  
    fmapc :: (FunctorConstraint f a, FunctorConstraint f b) => (a -> b) -> f a -> f b 
instance Functor f => ConstrainedFunctor (Unconstraining f) where 
    type FunctorConstraint (Unconstraining f) = Unconstrained 
    fmapc = fmap  

class ConstraintFunctor f where 
    weakenConstraint :: (forall x . c x => c' x) => f c a -> f c' a
