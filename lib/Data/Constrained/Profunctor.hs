module Data.Constrained.Profunctor where 

import Data.Kind ( Constraint )
import Data.Profunctor ( Profunctor(dimap) ) 
import Data.Unconstrained ( Unconstraining2 )

class ConstraintProfunctor p where 
    extendConstraints :: (forall x . c' x => c x, forall x . d x => d' x) => p c d x y -> p c' d' x y 
class ConstrainedProfunctor p where 
    type ProfunctorConstraint p x y :: Constraint 
    dimapc :: (ProfunctorConstraint p x y, ProfunctorConstraint p x' y') => (x' -> x) -> (y -> y') -> p x y -> p x' y'
instance Profunctor p => ConstrainedProfunctor (Unconstraining2 p) where 
    type ProfunctorConstraint (Unconstraining2 p) x y = ()
    dimapc = dimap 