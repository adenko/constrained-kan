module Data.Unconstrained where 

import Data.Bifunctor ( Bifunctor ) 
import Data.Functor.Contravariant ( Contravariant ) 
import Data.Profunctor ( Profunctor ) 

class Unconstrained x 
instance Unconstrained x 
newtype Unconstraining f x = Unconstraining (f x) deriving (Contravariant, Functor, Applicative, Monad)
newtype Unconstraining2 p x y = Unconstraining2 (p x y) deriving (Contravariant, Functor, Applicative, Monad, Bifunctor, Profunctor)